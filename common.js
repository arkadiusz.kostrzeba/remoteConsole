function $(selector, container) {
    return (container || document).querySelector(selector);
}