<?php
class OutputBufferControl
{
    private static $instance;
    private $bufferingOff;
    private function __construct() {}
    private function __clone() {}
    public static function getInstance() {
        if(self::$instance === null) {
            self::$instance = new OutputBufferControl();
        }
        return self::$instance;
    }

    public function turnOffBuffering()
    {
        if(!$this->bufferingOff){
            ob_implicit_flush(true);
            set_time_limit(0);
            $this->bufferingOff = true;
        }
    }

    public function turnOnBuffering()
    {
        if($this->bufferingOff) {
            ob_implicit_flush(false);
            set_time_limit(15);
            $this->bufferingOff = false;
        }
    }

    public function testOutput()
    {
        for($i=0;$i<10;$i++)
        {
            ob_start();
            echo "$i<br>";
            sleep(1);
            ob_end_flush();
            ob_flush();
        }
    }
}