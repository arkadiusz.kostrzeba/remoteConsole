<?php

class StreamedResponse
{
	public function __constructor()
	{
		header('Content-Encoding: chunked; Transfer-Encoding: chunked; Content-Type: text/html; Connection: keep-alive;');
		flush();
		ob_flush();
	}
	
	public function dumpChunk($chunk) {
		echo sprintf("%x\r\n%x\r\n", strlen($chunk), $chunk);
		flush();
		ob_flush();
	}
	
	public function closeConnection()
	{
		echo "0\r\n";
	}
}

$stream = new StreamedResponse();

$i = 0;
while($i < 10) {
	$stream->dumpChunk("$i");
	$i++;
	sleep(1);
}

$stream->closeConnection();
