(function(){

var _ = self.Tab = function(config) {
	this.tab = document.createElement('div');
	this.tab.setAttribute('id', config.id);
	this.init(config.menuList, 30);
	
    this.events = config.events;	
	return this.tab;
		
};

_.prototype = {
	init: function(menuElements, menuSize){
		
		this.menu = document.createElement('div');
		this.menu.size = menuSize;
		this.menu.style = `width: ${this.menu.size}px; height: 100px; float: right; background: #CDDC39;`;
		
		for(let i = 0; i < menuElements.length; i++){
			this.addMenuElement(menuElements[i]);
		}
		this.tab.appendChild(this.menu);
		
		this.tab.showContent = () => {
			// show content of the tab - hide all tools
			console.log('displayed');
			this.events.showContent();
		};
	},
	
	addMenuElement: function(object){
		var icon = document.createElement('img');
		icon.src = object.iconSrc;
		icon.width = this.menu.size - 2;
		icon.height = this.menu.size - 2;
		this.menu.appendChild(icon);
		
		icon.addEventListener('click', (function(){
			if(object.page){
				object.page.toggle();
				return;
			}
			this.tab.showContent()
		}).bind(this));
	},	
};

})();



(function(){

var _ = self.ToolBar = function() {
	this.createGrid();
	console.log();
	this.console = new consoleCmp(this.tabs.consoleTab, '/stream.php');
};

_.prototype = {
	createGrid: function(){
		var fragment = document.createElement('div');
		fragment.style = "position: fixed; bottom: 0; left: 0; width: 100%; height: 100px; border: 1px solid #70d05f; background: #c5e2c0;";
		
		this.tabs = {
			consoleTab: new Tab({
				id: 'console',
				menuList: [
					{
						iconSrc: 'img/gear.png',
						iconId: 'set',
						page: "This is settings page for current tab.",
					},
					{
						iconSrc: 'img/button_play_1-512.png',
						iconId: 'start',
						page: null,
					}					
				],
				events: {
					'showContent': () => {this.runTailLog();},
				}
			}),
		};
		
		for( let key of Object.keys(this.tabs)){		
			fragment.appendChild(this.tabs[key]);
		}
		
		
		document.body.appendChild(fragment);
	},
	
	runTailLog: function(){
		this.console.sendCommand('');
	},
};

})();




